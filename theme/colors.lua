local.tokyoCol = {}

--[Tokyo Night Storm]--
tokyoCol.foreground = "#a9b1d6"
tokyoCol.background = "#24283b"
tokyoCol.red = "#f7768e"
tokyoCol.yellow = "#e0af68"
tokyoCol.green = "#73daca"
tokyoCol.cyan = "#7dcfff"
tokyoCol.blue = "#7aa2f7"
tokyoCol.magenta = "#bb9af7"
tokyoCol.white = "#c0caf5"
tokyoCol.black = "#414868"

return tokyoCol